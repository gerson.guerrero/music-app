export const ACCOUNTS_ENDPOINT = 'https://accounts.spotify.com'
export const BASE_URL = `https://api.spotify.com/v1`
export const CLIENT_ID = process.env.REACT_APP_CLIENT_ID
export const CLIENT_SECRET = process.env.REACT_APP_CLIENT_SECRET
export const REDIRECT_URI = process.env.REACT_APP_DOMAIN_URL
export const TOKEN_RESPONSE_TYPE = 'token'
export const CODE_RESPONSE_TYPE = 'code'

export const SCOPES = [
  'user-read-playback-state',
  'user-read-currently-playing',

  'user-read-private',
  'user-read-email',

  'user-follow-read',

  'user-library-modify',
  'user-library-read',

  'user-read-playback-position',
  'user-top-read',

  'playlist-modify-private',
  'playlist-read-private',
  'playlist-modify-public',
]
