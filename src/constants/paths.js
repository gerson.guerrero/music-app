const Paths = {
  HOME: '/',
  LOGIN: '/login',
  PLAYLIST: '/playlist',
}

export default Paths
