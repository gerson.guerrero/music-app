import axios from 'axios'
import { BASE_URL } from '../constants/credentialsSpotify'
import {
  convertMiliseconds,
  showAlert,
  handleErrorTimeOut,
} from '../helpers/helpers'
import {
  getFormattedTracks,
  getFormattedPlaylist,
  getFormattedPlaylists,
} from '../helpers/api'

const data = {
  lists: [],
  selected: null,
}

const GET_PLAYLISTS = 'GET_PLAYLISTS'
const SELECT_PLAYLIST = 'SELECT_PLAYLIST'
const CLEAN_PLAYLIST_SELECTED = 'CLEAN_PLAYLIST_SELECTED'
const LOGOUT = 'LOGOUT'

const playlists = (state = data, action) => {
  switch (action.type) {
    case GET_PLAYLISTS:
      return {
        ...state,
        lists: action.payload,
      }
    case SELECT_PLAYLIST:
      return {
        ...state,
        selected: action.payload,
      }
    case CLEAN_PLAYLIST_SELECTED:
      return {
        ...state,
        selected: null,
      }
    case LOGOUT:
      return data
    default:
      return state
  }
}

export const doSetPlaylists = () => async (dispatch, getState) => {
  try {
    const token = getState().authentication.access_token
    const result = await axios.get(`${BASE_URL}/me/playlists`, {
      headers: { Authorization: `Bearer ${token}` },
    })

    const lists = getFormattedPlaylists(result.data.items)

    dispatch({
      type: GET_PLAYLISTS,
      payload: lists,
    })
  } catch (error) {
    handleErrorTimeOut(error, () => dispatch({ type: LOGOUT }))
  }
}

export const doSetSelectedPlaylists =
  (playlistId) => async (dispatch, getState) => {
    try {
      const token = getState().authentication.access_token
      dispatch({ type: CLEAN_PLAYLIST_SELECTED })
      const result = await axios.get(`${BASE_URL}/playlists/${playlistId}`, {
        headers: { Authorization: `Bearer ${token}` },
      })

      const playlist = {
        ...result.data,
        tracks: {
          ...result.data.tracks,
          items: await validateFavorites(
            // se limita a 50 tracks por requisitos de otros endpoints.
            result.data.tracks.items.slice(0, 50),
            token,
            dispatch
          ),
        },
      }
      const playlistSelected = getFormattedPlaylist(playlist)
      dispatch({ type: SELECT_PLAYLIST, payload: playlistSelected })
    } catch (error) {
      handleErrorTimeOut(error, () => dispatch({ type: LOGOUT }))
      const { status } = JSON.parse(JSON.stringify(error))
      if (status === 404) {
        showAlert('No existe la playlist requerida.', 3000, 'error', () => {
          window.location.replace('/')
        })
      }
    }
  }

export const doCleanSelected = () => async (dispatch, getState) => {
  dispatch({
    type: CLEAN_PLAYLIST_SELECTED,
  })
}

export const doLoadFavorites = () => async (dispatch, getState) => {
  try {
    dispatch({ type: CLEAN_PLAYLIST_SELECTED })

    const token = getState().authentication.access_token
    const infoUser = getState().user
    const result = await axios.get(`${BASE_URL}/me/tracks?limit=50`, {
      headers: { Authorization: `Bearer ${token}` },
    })

    const tracksWithFavorites = await validateFavorites(
      // se limita a 50 tracks por requisitos de otros endpoints.
      result.data.items.slice(0, 50),
      token,
      dispatch
    )

    const alsoFavorites = tracksWithFavorites.filter(
      (item) => item.track.is_favorite
    )

    const { tracks, duration } = getFormattedTracks(alsoFavorites)

    const list = {
      id: 'favorites',
      name: 'Canciones que te gustan',
      owner: infoUser.name,
      image: 'https://t.scdn.co/images/3099b3803ad9496896c43f22fe9be8c4.png',
      description: '',
      amountTracks: tracks.length,
      duration: convertMiliseconds(duration),
      tracks: tracks,
    }
    dispatch({ type: SELECT_PLAYLIST, payload: list })
  } catch (error) {
    handleErrorTimeOut(error, () => dispatch({ type: LOGOUT }))
  }
}

const validateFavorites = async (tracks, token, dispatch) => {
  try {
    const ids = tracks.map((item) => item.track.id).join(',')
    const areFavorites = await axios.get(`${BASE_URL}/me/tracks/contains`, {
      params: { ids: ids },
      headers: { Authorization: `Bearer ${token}` },
    })

    return tracks.map((item, ind) => ({
      added_at: item.added_at,
      track: {
        ...item.track,
        is_favorite: areFavorites.data[ind],
      },
    }))
  } catch (error) {
    handleErrorTimeOut(error, () => dispatch({ type: LOGOUT }))
  }
}

export default playlists
