import { refreshAccessToken } from '../helpers/authentication'

const data = {
  access_token: '',
  token_type: '',
  scope: '',
  expires_in: 0,
  refresh_token: '',
  isLogged: false,
}

const SET_AUTHENTICATION_INFO = 'SET_AUTHENTICATION_INFO'
const LOGOUT = 'LOGOUT'

const authentication = (state = data, action) => {
  switch (action.type) {
    case SET_AUTHENTICATION_INFO:
      return {
        ...state,
        ...action.payload,
        isLogged: true,
      }
    case LOGOUT:
      return data
    default:
      return state
  }
}

export const doSetSessionInfo = (accessData) => async (dispatch, getState) => {
  if (!accessData) {
    const { refresh_token, expires_in, access_token } =
      getState().authentication
    if (access_token) {
      // Se refrescará el token 5 mins antes que expire.
      const timeForRefresh = (expires_in - 300) * 1000
      setTimeout(async () => {
        console.log('Se refrescará el token...')
        const resp = await refreshAccessToken(refresh_token)
        dispatch({
          type: SET_AUTHENTICATION_INFO,
          payload: resp,
        })
      }, timeForRefresh)
    }
  } else {
    dispatch({
      type: SET_AUTHENTICATION_INFO,
      payload: accessData,
    })
  }
}

export const doLogout = () => (dispatch, getState) => dispatch({ type: LOGOUT })

export default authentication
