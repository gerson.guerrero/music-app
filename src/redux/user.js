import axios from 'axios'
import { BASE_URL } from '../constants/credentialsSpotify'
import { handleErrorTimeOut } from '../helpers/helpers'

const data = {
  id: '',
  name: '',
  image: null,
}

const GET_USER_INFO = 'GET_USER_INFO'
const LOGOUT = 'LOGOUT'

const user = (state = data, action) => {
  switch (action.type) {
    case GET_USER_INFO:
      return {
        ...state,
        ...action.payload,
      }
    case LOGOUT:
      return data
    default:
      return state
  }
}

export const doSetUserInfo = () => async (dispatch, getState) => {
  try {
    const token = getState().authentication.access_token
    const result = await axios.get(`${BASE_URL}/me`, {
      headers: { Authorization: `Bearer ${token}` },
    })
    if (result.status === 200) {
      const imageProfile = result.data.images[0].url
      const { display_name, id } = result.data
      dispatch({
        type: GET_USER_INFO,
        payload: { name: display_name, id, image: imageProfile },
      })
    }
  } catch (error) {
    handleErrorTimeOut(error, () => dispatch({ type: LOGOUT }))
  }
}

export default user
