import { applyMiddleware, combineReducers, compose, createStore } from 'redux'
import { persistReducer } from 'redux-persist'
import storage from 'redux-persist/lib/storage'

import thunk from 'redux-thunk'

import authentication from './authentication'
import playlists from './playlists'
import tracks from './tracks'
import user from './user'

const persistConfig = {
  key: 'root',
  storage,
}

const rootReducer = combineReducers({
  authentication: authentication,
  playlists: playlists,
  tracks: tracks,
  user: user
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const persistedReducer = persistReducer(persistConfig, rootReducer)

const store = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware(thunk))
)

export default store
