import axios from 'axios'
import { BASE_URL, SCOPES } from '../constants/credentialsSpotify'
import { getFormattedTracks } from '../helpers/api'
import { handleErrorTimeOut } from '../helpers/helpers'

const data = {
  tracks: [],
  selected: null,
}

const GET_TRACKS = 'GET_TRACKS'
const SELECT_TRACK = 'SELECT_TRACK'
const CLEAN_TRACK_SELECTED = 'CLEAN_TRACK_SELECTED'
const LOGOUT = 'LOGOUT'

const tracks = (state = data, action) => {
  switch (action.type) {
    case GET_TRACKS:
      return {
        ...state,
        tracks: action.payload,
      }
    case SELECT_TRACK:
      return {
        ...state,
        selected: action.payload,
      }
    case CLEAN_TRACK_SELECTED:
      return {
        ...state,
        selected: null,
      }
    case LOGOUT:
      return data
    default:
      return state
  }
}

export const doSetTracks = () => async (dispatch, getState) => {
  try {
    const token = getState().authentication.access_token
    const result = await axios.get(`${BASE_URL}/me/tracks`, {
      headers: { Authorization: `Bearer ${token}` },
    })
    const resp = getFormattedTracks(result.data.items)
    dispatch({
      type: GET_TRACKS,
      payload: resp,
    })
  } catch (error) {
    handleErrorTimeOut(error, () => dispatch({ type: LOGOUT }))
  }
}

export const doSetSelectedTrack = (track) => async (dispatch, getState) => {
  dispatch({
    type: CLEAN_TRACK_SELECTED,
  })
  dispatch({
    type: SELECT_TRACK,
    payload: track,
  })
}

export const doCleanSelected = () => async (dispatch, getState) => {
  dispatch({
    type: CLEAN_TRACK_SELECTED,
  })
}

export const addToFavorites = (idTrack) => async (dispatch, getState) => {
  try {
    const token = getState().authentication.access_token

    await axios.put(
      `${BASE_URL}/me/tracks?scope=${SCOPES.join('%20')}`,
      { ids: [`${idTrack}`] },
      {
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
        },
      }
    )
  } catch (error) {
    handleErrorTimeOut(error, () => dispatch({ type: LOGOUT }))
  }
}

export const removeFromFavorites = (idTrack) => async (dispatch, getState) => {
  try {
    const token = getState().authentication.access_token

    await axios.delete(`${BASE_URL}/me/tracks?scope=${SCOPES.join('%20')}`, {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json',
      },
      data: {
        ids: [`${idTrack}`],
      },
    })
  } catch (error) {
    handleErrorTimeOut(error, () => dispatch({ type: LOGOUT }))
  }
}

export default tracks
