import LoadingPage from './components/pages/LoadingPage'
import { BrowserRouter as Router } from 'react-router-dom'
import { createTheme } from '@mui/material'
import { PersistGate } from 'redux-persist/integration/react'
import { persistStore } from 'redux-persist'
import { Provider } from 'react-redux'
import store from './redux/store'
import { ThemeProvider } from '@emotion/react'
import { Toaster } from 'react-hot-toast';

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
})

const persistor = persistStore(store)

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ThemeProvider theme={darkTheme}>
          <Router>
            <Toaster />
            <LoadingPage />
          </Router>
        </ThemeProvider>
      </PersistGate>
    </Provider>
  )
}

export default App
