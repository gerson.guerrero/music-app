import moment from 'moment'
import toast from 'react-hot-toast'

const convertMiliseconds = (miliseconds, format) => {
  var days, hours, minutes, seconds, total_hours, total_minutes, total_seconds

  total_seconds = parseInt(Math.floor(miliseconds / 1000))
  total_minutes = parseInt(Math.floor(total_seconds / 60))
  total_hours = parseInt(Math.floor(total_minutes / 60))
  days = parseInt(Math.floor(total_hours / 24))

  seconds = parseInt(total_seconds % 60)
  minutes = parseInt(total_minutes % 60)
  hours = parseInt(total_hours % 24)

  switch (format) {
    case 's':
      return total_seconds
    case 'm':
      return total_minutes
    case 'h':
      return total_hours
    case 'd':
      return days
    case 'm:s':
      return `${minutes}: ${seconds < 10 ? '0' + seconds : seconds}`
    default:
      return `${hours > 0 ? hours + ' hr ' : ''}${
        minutes > 0 ? minutes + ' min ' : ''
      }${hours > 0 ? '' : seconds > 0 ? seconds + ' seg' : ''}`
  }
}

const getFormattedDate = (date) => moment(date).format('DD MMM YYYY')

const showAlert = (msg, time, variant, action) => {
  const styleAlert = {
    boxShadow: '0 4px 60px rgba(255, 255, 255, 0.2)',
    fontFamily: 'Montserrat',
    fontWeight: 600,
    color: 'white',
  }

  switch (variant) {
    case 'success':
      toast.success(
        msg,
        { style: { ...styleAlert, backgroundColor: '#b4e3aecc' } },
        time
      )
      break
    case 'error':
      toast.error(
        msg,
        { style: { ...styleAlert, backgroundColor: '#e3aeaecc' } },
        time
      )
      break
    default:
      toast(msg, { style: styleAlert }, time)
  }
  if (action) {
    setTimeout(action, time)
  }
}

const handleErrorTimeOut = (error, action) => {
  const { status } = JSON.parse(JSON.stringify(error))
  if (status === 401) {
    showAlert('Tiempo de espera agotado⏲.', 2000, 'error', action)
  }
}

export { convertMiliseconds, getFormattedDate, showAlert, handleErrorTimeOut }
