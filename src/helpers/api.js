import { convertMiliseconds, getFormattedDate } from './helpers'

const getFormattedTrack = (item, pos = 0) => {
  const { track, added_at } = item
  const artists = track.artists
    ? track.artists.map((artist) => artist.name).join(', ')
    : ''
  return {
    id: track.id,
    number: pos + 1,
    name: track.name,
    artists: artists,
    date: getFormattedDate(added_at),
    duration: track.duration_ms,
    durationMin: convertMiliseconds(track.duration_ms, 'm:s'),
    album: track.album.name,
    image: track.album.images[0].url,
    uri: track.uri,
    explicit: track.explicit,
    isFavorite: track.is_favorite,
  }
}

const getFormattedTracks = (tracks) => {
  let duration = 0
  const tracksFormatted = tracks.map((item, pos) => {
    const trackFormatted = getFormattedTrack(item, pos)
    duration += trackFormatted.duration
    return trackFormatted
  })
  return {
    tracks: tracksFormatted,
    duration: duration,
  }
}

const getFormattedPlaylist = (item) => {
  const { tracks, duration } = item.tracks.items
    ? getFormattedTracks(item.tracks.items)
    : { tracks: null, duration: 0 }
  return {
    id: item.id,
    name: item.name,
    image: item.images[0].url,
    description: item.description,
    owner: item.owner.display_name,
    amountTracks: tracks ? item.tracks.total : 0,
    tracks: tracks,
    duration: convertMiliseconds(duration),
  }
}

const getFormattedPlaylists = (items) => {
  return items
    .map((playlist) => getFormattedPlaylist(playlist))
    .filter((playlist) => playlist.name)
}

export {
  getFormattedTrack,
  getFormattedTracks,
  getFormattedPlaylist,
  getFormattedPlaylists,
}
