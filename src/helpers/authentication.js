import axios from 'axios'
import base64 from 'base-64'
import {
  ACCOUNTS_ENDPOINT,
  CLIENT_ID,
  REDIRECT_URI,
  SCOPES,
  CODE_RESPONSE_TYPE,
  CLIENT_SECRET,
} from '../constants/credentialsSpotify'
import { handleErrorTimeOut } from '../helpers/helpers'

const getAuthorization = () => {
  window.location = `${ACCOUNTS_ENDPOINT}/authorize?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&scope=${SCOPES.join(
    '%20'
  )}&response_type=${CODE_RESPONSE_TYPE}&show_dialog=true`
}

const getAccessDataFromUrl = async () => {
  const params = window.location.search
    .substring(1)
    .split('&')
    .reduce((initial, item) => {
      let parts = item.split('=')
      initial[parts[0]] = decodeURIComponent(parts[1])
      return initial
    }, {})

  if (params.code) {
    return getAccessToken(params.code)
  }
}

const getAccessToken = async (code) => {
  try {
    const params = {
      code: code,
      redirect_uri: REDIRECT_URI,
      grant_type: 'authorization_code',
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
    }
    const resp = await axios.post(
      `${ACCOUNTS_ENDPOINT}/api/token`,
      null,
      { params },
      {
        headers: {
          Authorization:
            'Basic ' + base64.encode(CLIENT_ID + ':' + CLIENT_SECRET),
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      }
    )

    return resp.data
  } catch (error) {
    handleErrorTimeOut(error)
  }
}

const refreshAccessToken = async refreshToken => {
  try {
    const params = {
      refresh_token: refreshToken,
      redirect_uri: REDIRECT_URI,
      grant_type: 'refresh_token',
      client_id: CLIENT_ID,
      client_secret: CLIENT_SECRET,
    }
    const resp = await axios.post(
      `${ACCOUNTS_ENDPOINT}/api/token`,
      null,
      { params },
      {
        headers: {
          Authorization:
            'Basic ' + base64.encode(CLIENT_ID + ':' + CLIENT_SECRET),
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      }
    )

    console.log('nuevo info token: ', resp.data)
    return resp.data
  } catch (error) {
    handleErrorTimeOut(error)
  }
}


export { getAccessDataFromUrl, getAuthorization, refreshAccessToken }
