import { render, screen } from '@testing-library/react'
import Footer from '../../../components/organisms/Footer'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import '@testing-library/jest-dom'

const middlewares = [thunk]
const mockStore = configureStore(middlewares)

const initState = {
  tracks: {
    selected: {
      uri: 'uri_test',
    },
  },
}

let store = mockStore(initState)

describe('Pruebas en <Footer />', () => {
  beforeEach(() => {
    store = mockStore(initState)
  })

  test('Al NO tener canción seleccionada el contenedor debe tener la clase footer__hidden.', () => {
    store = mockStore({
      tracks: {
        selected: null,
      },
    })
    const { container } = render(
      <Provider store={store}>
        <Footer />
      </Provider>
    )

    container.getElementsByClassName('footer__hidden')
  })

  test('Teniendo una canción seleccionada el src debe tener el uri insertado.', () => {
    render(
      <Provider store={store}>
        <Footer />
      </Provider>
    )
    const URL_EMBED = 'https://open.spotify.com/embed?uri='

    const iframe = screen.getByTitle('Repoductor')
    expect(iframe).toBeInTheDocument()
    expect(iframe).toHaveProperty('src', `${URL_EMBED}uri_test`)
  })
})
