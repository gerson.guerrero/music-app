import { prettyDOM, render, screen } from '@testing-library/react'
import Sidebar from '../../../components/organisms/Sidebar/Sidebar'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { MemoryRouter } from 'react-router-dom'

const middlewares = [thunk]
const mockStore = configureStore(middlewares)

const initState = {
  playlists: {
    lists: [],
    selected: null,
  },
}
let store = mockStore(initState)

// Se hace el mock del useDispatch()
const mockedDispatcher = jest.fn()

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockedDispatcher,
}))

describe('Pruebas en <Sidebar />', () => {
  beforeEach(() => {
    store = mockStore(initState)
  })
  test('se debe visualizar correctamente el logo de spotify, la opción Favoritos y el titulo de PLAYLISTS.', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <Sidebar />
        </MemoryRouter>
      </Provider>
    )
    const image = screen.getByRole('img')
    expect(image).toBeDefined()
    expect(image).toHaveProperty('src')
    expect(image.src).toMatch(/spotify_white.png/)

    screen.getByText('PLAYLISTS')
    
    screen.getByTestId('FavoriteBorderOutlinedIcon')
    screen.getByText('Favoritos')
  })
  test('si existen playlists deben mostrarse esa misma cantidad de opciones con su informacion correspondiente.', () => {
    const lists = [
      { id: 1, name: 'Playlist 1' },
      { id: 2, name: 'Playlist 2' },
      { id: 3, name: 'Playlist 3' }
    ]

    store = mockStore({
      playlists: {
        lists
      }
    })
    render(
      <Provider store={store}>
        <MemoryRouter>
          <Sidebar />
        </MemoryRouter>
      </Provider>
    )
    
    const liElements = screen.getAllByRole('listitem')
    expect(liElements).toHaveLength(lists.length + 1)

    lists.map(list => {
      screen.getByText(list.name)
    })
  })
  test('si NO existen playlists debe mostrarse el texto predeterminado.', () => {
    render(
      <Provider store={store}>
        <MemoryRouter>
          <Sidebar />
        </MemoryRouter>
      </Provider>
    )
    screen.getByText('No hay playlists')
  })
})
