import { fireEvent, render, screen } from '@testing-library/react'
import { Provider } from 'react-redux'
import configureStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import '@testing-library/jest-dom'
import Navbar from '../../../components/organisms/Navbar'

const middlewares = [thunk]
const mockStore = configureStore(middlewares)

const user = {
  id: 'jairguerrero',
  name: 'Gerson Guerrero',
  image: 'https://i.scdn.co/image/ab6775700000ee85df5528b406ff8b3091cca84a',
}

let store = mockStore({ user })

// Se hace el mock del useDispatch()
const mockedDispatcher = jest.fn()

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockedDispatcher,
}))

describe('Pruebas en <Navbar />', () => {
  beforeEach(() => {
    store = mockStore({ user })
  })

  test('se debe cargar la imagen y el nombre del usuario en sesion.', () => {
    render(
      <Provider store={store}>
        <Navbar />
      </Provider>
    )

    //Validamos que se cargue la imagen del usuario.
    const imgProfile = screen.getByRole('img')
    expect(imgProfile).toHaveProperty('src', user.image)

    // Validamos que se cargue el nombre del usuario.
    screen.getByText(user.name)
  })

  test('debe funcionar correctamente la funcion del botón de salir y sus propiedades.', () => {
    render(
      <Provider store={store}>
        <Navbar />
      </Provider>
    )

    const logoutButton = screen.getAllByRole('button')[1]
    // validamos que exista el icono del boton salir.
    expect(screen.getByTestId('CancelRoundedIcon')).toBeDefined()
    // validamos el texto del botón salir.
    expect(logoutButton).toHaveTextContent('Salir')
    // validamos el click en el boton.
    fireEvent.click(logoutButton)
    expect(mockedDispatcher).toHaveBeenCalled()
  })
})
