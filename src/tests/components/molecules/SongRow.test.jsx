import { fireEvent, prettyDOM, render, screen } from '@testing-library/react'
import SongRow from '../../../components/molecules/SongRow/SongRow'

const track = {
  id: '1yFAfozEjcUjnG2OgN6X4S',
  number: 1,
  image: 'https://i.scdn.co/image/ab67616d0000b273b6851509dd5b8efbfb95cea0',
  name: 'Busco Alguien Que Me Quiera',
  explicit: false,
  artists: 'El Afinaito',
  album: 'Un Beso Desde Cartagena Con las Mejores Champetas',
  date: '26 Jan 2022',
  isFavorite: false,
  durationMin: '4: 27',
  uri: 'spotify:track:1yFAfozEjcUjnG2OgN6X4S',
}

// Se hace el mock del useDispatch()
const mockedDispatcher = jest.fn()

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: () => mockedDispatcher,
}))

describe('Pruebas en <SongRow />', () => {
  test('cuando no tiene información de la canción no debe mostrar informacion relevante.', () => {
    render(<SongRow />)
    // screen.debug()
  })

  test('cuando la propiedad title está en true debe mostrar los titulos de las columnas.', () => {
    render(<SongRow title={true} />)

    screen.getByText('#')
    screen.getByText('TÍTULO')
    screen.getByText('ÁLBUM')
    screen.getByText('FECHA INCORPORACIÓN')
  })

  test('cuando hay información relacionada a una canción se debe mostrar las propiedades de la misma.', () => {
    render(<SongRow track={track} />)

    screen.getByText(track.number)
    expect(screen.getByRole('img')).toHaveProperty('src', track.image)
    screen.getByText(track.name)
    screen.getByText(track.artists)
    screen.getByText(track.album)
    screen.getByText(track.date)
    screen.getByText(track.durationMin)
    // la canción está fuera de la lista de favoritos
    expect(screen.getByTestId('FavoriteBorderOutlinedIcon')).toBeDefined()
  })

  test('cuando la canción esté seleccionada debe mostrar el icono de play.', () => {
    render(<SongRow track={track} isSelected={true} />)

    expect(screen.getByTestId('PlayArrowIcon')).toBeDefined()
  })

  test('cuando la canción NO está en la lista de favoritos, debe tener el icono vacio y permitir el click.', () => {
    render(
      <SongRow track={{ ...track, isFavorite: false }} isSelected={true} />
    )

    const favButton = screen.getByTestId('FavoriteBorderOutlinedIcon')
    fireEvent.click(favButton)
    expect(mockedDispatcher).toBeCalled()
  })

  test('cuando la canción está en la lista de favoritos, debe tener el icono lleno y permitir el click.', () => {
    render(<SongRow track={{ ...track, isFavorite: true }} isSelected={true} />)

    const noFavButton = screen.getByTestId('FavoriteIcon')
    expect(noFavButton).toBeDefined()

    fireEvent.click(noFavButton)
    expect(mockedDispatcher).toBeCalled()
  })
})
