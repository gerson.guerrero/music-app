import { render, screen } from '@testing-library/react'
import CardInfo from '../../../components/molecules/CardInfo/CardInfo'

const data = {
  image: 'https://t.scdn.co/images/3099b3803ad9496896c43f22fe9be8c4.png',
  name: 'Canciones que te gustan',
  description: 'description',
  duration: '1 hr 31 min',
  amountTracks: 21,
  owner: 'Gerson Guerrero',
}

describe('Pruebas en <CardInfo />', () => {
  test('sin pasarle información de la playlist debe cargar solo el titulo PLAYLIST pero renderizar bien el componente.', () => {
    render(<CardInfo />)
    screen.getByText('PLAYLIST')
  })
  
  test('cuando hay información relacionada a una playlist se debe mostrar las propiedades de la misma', () => {
    render(
      <CardInfo
        image={data.image}
        name={data.name}
        description={data.description}
        duration={data.duration}
        amountTracks={data.amountTracks}
        owner={data.owner}
      />
    )
    screen.getByText('PLAYLIST')

    const img = screen.getByRole('img')
    expect(img).toHaveProperty('src', data.image)
    
    screen.getByText(data.name)
    screen.getByText(data.description)
    screen.getByText(data.owner)
    screen.getByText(`● ${data.amountTracks} ${data.amountTracks > 1 ? 'canciones' : 'canción'}, ${data.duration}`)
  })
})
