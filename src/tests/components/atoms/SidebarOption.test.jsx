import { render, screen, fireEvent } from '@testing-library/react'
import SidebarOption from '../../../components/atoms/SidebarOption'
import { MemoryRouter } from 'react-router-dom'
import FavoriteBorderOutlinedIcon from '@mui/icons-material/FavoriteBorderOutlined'
import '@testing-library/jest-dom'

const option = {
  title: 'Option test',
  redirect: '/hello',
  isSelected: true,
  icon: () => <FavoriteBorderOutlinedIcon />,
}

const mockedNavigator = jest.fn();

jest.mock("react-router-dom", () => ({
  ...jest.requireActual("react-router-dom"),
  useNavigate: () => mockedNavigator,
}));

describe('Pruebas en <SidebarOption />', () => {
  test('debe mostrarse el icono y un h4 con su title.', () => {
    render(
      <MemoryRouter>
        <SidebarOption title={option.title} Icon={option.icon} />
      </MemoryRouter>
    )
    expect(screen.getByTestId('FavoriteBorderOutlinedIcon')).toBeDefined()
    expect(screen.getByRole('heading')).toHaveTextContent(option.title)
  })

  test('debe mostrarse solo un <p /> con el title.', () => {
    render(
      <MemoryRouter>
        <SidebarOption title={option.title} />
      </MemoryRouter>
    )
    screen.getByText(option.title)
  })

  test('debe mostrarse un <p /> con el title y el elemento <li /> con la clase option__selected.', () => {
    render(
      <MemoryRouter>
        <SidebarOption title={option.title} isSelected={option.isSelected} />
      </MemoryRouter>
    )
    screen.getByText(option.title)
    expect(screen.getByRole('listitem')).toHaveClass('option__selected')
  })

  test('debe reconocer la accion de redireccionar al hacer click en el elemento.', () => {
    render(
      <MemoryRouter>
        <SidebarOption title={option.title} isSelected={option.isSelected} redirect={option.redirect}/>
      </MemoryRouter>
    )
    const listItem = screen.getByRole('listitem')
    fireEvent.click(listItem)
    expect(mockedNavigator).toHaveBeenCalled()
  })
})
