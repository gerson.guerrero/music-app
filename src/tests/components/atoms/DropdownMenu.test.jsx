import { render, screen, fireEvent } from '@testing-library/react'
import DropdownMenu from '../../../components/atoms/DropdownMenu'
import '@testing-library/jest-dom'

describe('Pruebas en <DropdownMenu />', () => {
  const testContent = <h1>Its works</h1>

  test('Haciendo click en el boton se debe mostrar el contenido', () => {
    render(<DropdownMenu content={testContent} />)

    const button = screen.getByRole('button')
    fireEvent.click(button)
    expect(screen.getByRole('heading')).toHaveTextContent('Its works')
  })
})
