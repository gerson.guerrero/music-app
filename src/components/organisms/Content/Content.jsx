import Navbar from '../Navbar'
import NotFoundPage from '../../pages/NotFoundPage'
import Paths from '../../../constants/paths'
import Playlist from '../../organisms/Playlist'
import { Routes, Route, Navigate } from 'react-router-dom'
import './Content.css'

const Content = () => {
  return (
    <div className="content">
      <Navbar />
      <Routes>
        <Route path={Paths.HOME} element={<Navigate to={Paths.PLAYLIST} />} />
        <Route path={Paths.PLAYLIST} element={<Playlist />}>
          <Route path=":id" element={<Playlist />} />
        </Route>
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </div>
  )
}

export default Content
