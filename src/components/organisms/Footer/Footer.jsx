import Skeleton from '@mui/material/Skeleton'
import { useSelector } from 'react-redux'
import './Footer.css'

const Footer = () => {
  const trackSelected = useSelector((store) => store.tracks.selected)

  return (
    <div className={`footer ${!trackSelected ? 'hidden__footer' : ''}`}>
      {trackSelected ? (
        <iframe
          title="Repoductor"
          src={`https://open.spotify.com/embed?uri=${trackSelected.uri}`}
          width="100%"
          height="100%"
          frameBorder="0"
          allowtransparency="false"
          allow="encrypted-media"
        />
      ) : (
        <div className="footer_container">
          <div className="footer__img">
            <Skeleton variant="rectangular" width="80px" height="100%" />
          </div>
          <div className="footer__info">
            <div>
              <Skeleton variant="text" width="40%" />
              <Skeleton variant="text" width="35%" />
            </div>
            <Skeleton component="p" variant="text" width="95%" height="10%" />
          </div>
        </div>
      )}
    </div>
  )
}

export default Footer
