import Divider from '@mui/material/Divider'
import { doSetSelectedPlaylists, doLoadFavorites } from '../../../redux/playlists'
import SongRow from '../../molecules/SongRow'
import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import './Playlist.css'
import CardInfo from '../../molecules/CardInfo'

const Playlist = () => {
  const params = useParams()
  const dispatch = useDispatch()

  const playlist = useSelector((store) => store.playlists.selected)
  const trackSelected = useSelector((store) => store.tracks.selected)

  const [idCurrentList, setIdCurrentList] = useState(null)

  useEffect(() => {
    if (params.id) {
      dispatch(doSetSelectedPlaylists(params.id))
      setIdCurrentList(params.id)
    } else {
      dispatch(doLoadFavorites())
      setIdCurrentList(null)
    }
  }, [params.id])

  return (
    <div className="playlist">
      <CardInfo
        image={playlist?.image}
        name={playlist?.name}
        description={playlist?.description}
        duration={playlist?.duration}
        amountTracks={playlist?.amountTracks}
        owner={playlist?.owner}
      />
      <div className="playlist__songs">
        <SongRow title={true} />
        <Divider />
        {playlist?.tracks ? (
          playlist.tracks.map((track) => (
            <SongRow
              key={`${track.id}_${track.number}`}
              track={track}
              isSelected={trackSelected?.id === track.id}
              idCurrentPlaylist={idCurrentList}
            />
          ))
        ) : (
          <div>
            <SongRow />
            <SongRow />
            <SongRow />
            <SongRow />
            <SongRow />
            <SongRow />
          </div>
        )}
      </div>
    </div>
  )
}

export default Playlist
