import { doSetPlaylists } from '../../../redux/playlists'
import FavoriteBorderOutlinedIcon from '@mui/icons-material/FavoriteBorderOutlined'
import SidebarOption from '../../atoms/SidebarOption'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import Paths from '../../../constants/paths'
import './Sidebar.css'

const Sidebar = () => {
  const dispatch = useDispatch()
  const playlists = useSelector((store) => store.playlists.lists)

  const playlistSelected = useSelector((store) => store.playlists.selected)

  useEffect(() => {
    if (!playlists.length) {
      dispatch(doSetPlaylists())
    }
  }, [playlists])

  return (
    <div className="sidebar">
      <img
        className="sidebar__logo"
        src="/assets/images/spotify_white.png"
        alt="Logo Spotify"
      />
      <SidebarOption
        title="Favoritos"
        Icon={FavoriteBorderOutlinedIcon}
        redirect={Paths.PLAYLIST}
        isSelected={playlistSelected?.id === 'favorites'}
      />

      <br />
      <strong className="sidebar__title">PLAYLISTS</strong>
      <hr />

      {playlists.length ? (
        <ul className='sidebar__options'>
          {playlists.map((list) => (
            <SidebarOption
              key={list.id}
              title={list.name}
              redirect={`${Paths.PLAYLIST}/${list.id}`}
              isSelected={playlistSelected?.id === list.id}
            />
          ))}
        </ul>
      ) : (
        <div className='sidebar__empty'>No hay playlists</div>
      )}
    </div>
  )
}

export default Sidebar
