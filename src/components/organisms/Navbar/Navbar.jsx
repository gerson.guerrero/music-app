import Avatar from '@mui/material/Avatar'
import Button from '@mui/material/Button'
import CancelRoundedIcon from '@mui/icons-material/CancelRounded'
import { doLogout } from '../../../redux/authentication'
import { doSetUserInfo } from '../../../redux/user'
import DropdownMenu from '../../atoms/DropdownMenu'
import Sidebar from '../Sidebar'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import './Navbar.css'

const Navbar = () => {
  const dispatch = useDispatch()
  const user = useSelector((store) => store.user)

  useEffect(() => {
    if (!user.id) {
      dispatch(doSetUserInfo())
    }
  }, [user])

  const toLogout = () => {
    dispatch(doLogout())
  }

  return (
    <div className="navbar">
      <div className="navbar__left">
        <DropdownMenu
          content={<Sidebar />}
          position="left"
          classes="navbar__buttonMenu"
        />
        <Avatar src={user.image} alt="Imagen de usuario" />
        <h4>{user.name}</h4>
      </div>
      <div className="navbar__right">
        <Button
          variant="contained"
          onClick={toLogout}
          size="small"
          startIcon={<CancelRoundedIcon />}
          color="error"
        >
          Salir
        </Button>
      </div>
    </div>
  )
}

export default Navbar
