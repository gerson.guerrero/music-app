import Backdrop from '@mui/material/Backdrop'
import Button from '@mui/material/Button'
import CircularProgress from '@mui/material/CircularProgress'
import Grid from '@mui/material/Grid'
import { getAuthorization } from '../../../helpers/authentication'
import './LoginPage.css'

const LoginPage = ({ isLoading }) => {
  const toLogin = () => {
    getAuthorization()
  }
  return (
    <Grid
      container
      item
      direction="column"
      wrap="nowrap"
      justifyContent="center"
      alignItems="center"
      className="container"
    >
      <div className="spotify__logo">
        <img src="/assets/images/spotify_white.png" alt="Logo pequeño Spotify" />
      </div>
      {isLoading ? (
        <Backdrop
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={true}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
      ) : (
        <Button
          className="login__button"
          color="success"
          variant="contained"
          size="large"
          onClick={toLogin}
        >
          Login to Spotify
        </Button>
      )}
    </Grid>
  )
}

export default LoginPage
