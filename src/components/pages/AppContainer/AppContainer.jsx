import Content from '../../../components/organisms/Content'
import Footer from '../../../components/organisms/Footer'
import Sidebar from '../../../components/organisms/Sidebar'
import { useSelector } from 'react-redux'
import './AppContainer.css'

const AppContainer = () => {

  const trackSelected = useSelector((store) => store.tracks.selected)

  return (
    <div className="appContainer">
      <div className={`appContainer__body ${trackSelected ? 'appContainer__track' : ''}`}>
        <div className="appContainer__sidebar">
          <Sidebar />
        </div>
        <Content />
      </div>
      <Footer />
    </div>
  )
}

export default AppContainer
