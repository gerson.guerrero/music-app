import Grid from '@mui/material/Grid'
import Link from '@mui/material/Link'
import Paths from '../../constants/paths'
import { useNavigate } from 'react-router-dom'

const NotFoundPage = () => {
  const navigate = useNavigate()

  const goToHome = () => {
    navigate(Paths.HOME)
  }

  return (
    <Grid container direction="column" justifyContent="center" className="full">
      <Grid
        container
        item
        direction="column"
        justifyContent="center"
        alignItems="center"
      >
        <h1>Oops! No hay nada por aquí | 404</h1>
        <Link component="button" aria-label="menu" onClick={goToHome}>
          Volver al inicio
        </Link>
      </Grid>
    </Grid>
  )
}

export default NotFoundPage
