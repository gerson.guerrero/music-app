import AppContainer from './AppContainer'
import { doSetSessionInfo } from '../../redux/authentication'
import { doSetUserInfo } from '../../redux/user'
import { getAccessDataFromUrl } from '../../helpers/authentication'
import LoginPage from './LoginPage'
import Paths from '../../constants/paths'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router'

const LoadingPage = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { isLogged, access_token } = useSelector(
    (store) => store.authentication
  )
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    if (!isLogged) {
      const executeProcess = async () => {
        const accessData = await getAccessDataFromUrl()
        if (accessData?.access_token) {
          dispatch(doSetSessionInfo(accessData))
          dispatch(doSetUserInfo())
        }
        setIsLoading(false)
      }
      executeProcess()
    }
  }, [isLogged])

  useEffect(() => {
    if (!isLogged) {
      navigate(Paths.HOME)
    }
  }, [isLogged])

  useEffect(() => {
    if (access_token) {
      dispatch(doSetSessionInfo())
    }
  }, [access_token])

  return (
    <div>{isLogged ? <AppContainer /> : <LoginPage isLoading={isLoading} />}</div>
  )
}

export default LoadingPage
