import {
  addToFavorites,
  doSetSelectedTrack,
  removeFromFavorites,
} from '../../../redux/tracks'
import { doSetSelectedPlaylists, doLoadFavorites } from '../../../redux/playlists'
import ExplicitIcon from '@mui/icons-material/Explicit'
import FavoriteBorderOutlinedIcon from '@mui/icons-material/FavoriteBorderOutlined'
import FavoriteIcon from '@mui/icons-material/Favorite'
import QueryBuilderIcon from '@mui/icons-material/QueryBuilder'
import PlayArrowIcon from '@mui/icons-material/PlayArrow'
import Skeleton from '@mui/material/Skeleton'
import { useDispatch } from 'react-redux'
import './SongRow.css'

const SongRow = ({ track, title, isSelected, idCurrentPlaylist }) => {
  const dispatch = useDispatch()

  const updateList = () => {
    idCurrentPlaylist
      ? dispatch(doSetSelectedPlaylists(idCurrentPlaylist))
      : dispatch(doLoadFavorites())
  }

  const saveInFavorites = () => {
    dispatch(addToFavorites(track.id))
    updateList()
  }

  const removeFavorite = () => {
    dispatch(removeFromFavorites(track.id))
    updateList()
  }

  return (
    <div>
      {title ? (
        <div className="songRow__title">
          <h1 className="songRow__track">
            <span className="songRow__columnNumber">#</span>
            TÍTULO
          </h1>
          <h1 className="songRow__columnAlbum">ÁLBUM</h1>
          <h1 className="songRow__columnDate">FECHA INCORPORACIÓN</h1>
          <h1 className="songRow__columnFavorite clock__icon">
            <QueryBuilderIcon />
          </h1>
          <h1 className="songRow__columnFavorite fav__icon">
            <FavoriteIcon />
          </h1>
        </div>
      ) : track ? (
        <div
          className={`songRow ${isSelected ? 'track__selected' : ''}`}
          onClick={() => dispatch(doSetSelectedTrack(track))}
        >
          <div className="songRow__track">
            <div className="songRow__columnNumber">
              {isSelected ? <PlayArrowIcon /> : <p>{track.number}</p>}
            </div>
            <img className="songRow__album" src={track.image} alt="caratula de álbum" />
            <div className="songRow__info">
              <h5>{track.name}</h5>
              <p>
                {!track.explicit || <ExplicitIcon />}
                {track.artists}
              </p>
            </div>
          </div>
          <div className="songRow__columnAlbum">
            <p>{track.album}</p>
          </div>
          <div className="songRow__columnDate">
            <p>{track.date}</p>
          </div>
          <div className="songRow__columnFavorite">
            {track.isFavorite ? (
              <FavoriteIcon
                className="songRow__favoriteCheck"
                onClick={removeFavorite}
              />
            ) : (
              <FavoriteBorderOutlinedIcon
                className="songRow__favoriteCheck"
                onClick={saveInFavorites}
              />
            )}
            <p>{track.durationMin}</p>
          </div>
        </div>
      ) : (
        <div className="songRow">
          <div className="songRow__track">
            <Skeleton
              className="songRow__columnNumber"
              component="p"
              variant="text"
              width="2%"
            />
            <Skeleton className="songRow__album" width={40} height={40} />
            <Skeleton component="h1" className="songRow__info" width="40%" />
          </div>
          <div className="songRow__columnAlbum">
            <Skeleton variant="text" width="80%" />
          </div>
          <div className="songRow__columnDate">
            <Skeleton variant="text" width="80%" />
          </div>
          <div className="songRow__columnFavorite">
            <Skeleton variant="text" width="80%" />
          </div>
        </div>
      )}
    </div>
  )
}

export default SongRow
