import Skeleton from '@mui/material/Skeleton'
import useMediaQuery from '@mui/material/useMediaQuery'
import './CardInfo.css'

const CardInfo = ({
  image,
  name,
  description,
  duration,
  amountTracks,
  owner
}) => {
  const isTablet = useMediaQuery('(max-width:1050px)')

  return (
    <div
      className="cardInfo"
      style={{
        backgroundImage: isTablet
          ? `linear-gradient(to top, rgba(52, 49, 65, 0.6), rgba(82, 78, 104, 0.9)), url(${image})`
          : '',
      }}
    >
      <div className="cardInfo__cover">
        {!image ? (
          <Skeleton
            animation="wave"
            variant="rectangular"
            width="100%"
            height="100%"
          />
        ) : (
          <img src={image} alt="Portada de playlist" />
        )}
      </div>
      <div className="cardInfo__data">
        <strong>PLAYLIST</strong>
        <h1 className="cardInfo__dataTitle">
          {!name ? <Skeleton width="80%" height="100%" /> : name}
        </h1>

        {description ? (
          <p
            className="cardInfo__dataDescription"
            dangerouslySetInnerHTML={{ __html: description }}
          />
        ) : (
          <></>
        )}
        {owner || amountTracks || duration ? (
          <div>
            <strong>{owner}</strong>
            <span>{` ● ${amountTracks} ${
              amountTracks > 1 ? 'canciones' : 'canción'
            }, ${duration}`}</span>
          </div>
        ) : (
          <Skeleton width="50%" height="100%" />
        )}
      </div>
    </div>
  )
}

export default CardInfo
