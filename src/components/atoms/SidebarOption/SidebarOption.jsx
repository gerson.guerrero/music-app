import { useNavigate } from 'react-router-dom'
import './SidebarOption.css'

const SidebarOption = ({ title, Icon, redirect, isSelected }) => {
  const navigate = useNavigate()

  return (
    <li
      className={`sidebarOption ${isSelected ? 'option__selected' : ''}`}
      onClick={() => navigate(redirect)}
    >
      {Icon && <Icon className="sidebarOption__icon" />}
      {Icon ? <h4>{title}</h4> : <p>{title}</p>}
    </li>
  )
}

export default SidebarOption
